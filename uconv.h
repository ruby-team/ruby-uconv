/*
 * Unicode Conversion Library
 * 1997-1998 by yoshidam
 *
 */

#ifndef _UCONV_H
#define _UCONV_H

#include "ustring.h"
#include "ruby.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef VALUE (unknown_unicode)(unsigned long);
typedef VALUE (unknown_euc)(const unsigned char*);
typedef VALUE (unknown_sjis)(const unsigned char*);


int u2e_conv2(const unsigned char* u, int len, UString* e,
	      unknown_unicode unknown_u_conv,
              unknown_unicode u2e_hook);
int u2s_conv2(const unsigned char* u, int len, UString* s,
	      unknown_unicode unknown_u_conv,
              unknown_unicode u2s_hook);

int e2u_conv2(const unsigned char* e, UString* u,
	      unknown_euc unknown_e_conv,
              unknown_euc e2u_hook);
int s2u_conv2(const unsigned char* s, UString* u,
	      unknown_sjis unknown_s_conv,
              unknown_sjis s2u_hook);

unsigned short sjis2euc(unsigned short sjis);
unsigned short euc2sjis(unsigned short euc);


#ifdef __cplusplus
}
#endif

#endif
