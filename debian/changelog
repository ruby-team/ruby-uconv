ruby-uconv (0.6.1-4) UNRELEASED; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Remove unnecessary 'Testsuite: autopkgtest' header.
  * Fix day-of-week for changelog entries 0.3.1-1.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 08:08:22 +0530

ruby-uconv (0.6.1-3) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Antonio Terceiro ]
  * debian/tests/control: don't try rubygems dependency resolution; this
    package is not a proper Rubygems package (Closes: #819078)
  * debian/ruby-tests.rb: add a very simple unit test

 -- Antonio Terceiro <terceiro@debian.org>  Thu, 24 Mar 2016 19:21:51 -0300

ruby-uconv (0.6.1-2) unstable; urgency=medium

  * Team upload.
  * refreshed debian/ with dh-make-ruby
  * d/control:
   - removed transitional packages (Closes: #735717)
   - bumped dpkg compat level to 9

 -- Jonas Genannt <genannt@debian.org>  Fri, 21 Aug 2015 14:54:31 +0200

ruby-uconv (0.6.1-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.4.

 -- Taku YASUI <tach@debian.org>  Thu, 15 Aug 2013 19:39:45 +0900

ruby-uconv (0.5.3-2) unstable; urgency=low

  * Ruby package transition
    - Change package name to ruby-uconv
  * Change maintainer to Debian Ruby Extras Maintainers.
    - akira yamada <akira@debian.org> and Taku YASUI <tach@debian.org>
      are uploaders.
  * Commit debian source to git.debian.org.
    - git://git.debian.org/pkg-ruby-extras/ruby-uconv.git
    - Add Vcs metadata to debian/control.
  * Change source format to '3.0 (quilt)'.
  * Bump Standards-Verson to 3.9.3.
  * Use gem2deb to build package.

 -- Taku YASUI <tach@debian.org>  Sun, 06 May 2012 15:27:44 +0900

libuconv-ruby (0.5.3-1) unstable; urgency=low

  * new upstream release
  * ruby1.9.1 support  (Closes: 584140)

 -- akira yamada <akira@debian.org>  Thu, 10 Jun 2010 08:01:53 +0900

libuconv-ruby (0.4.12-3) unstable; urgency=high

  * libuconv-ruby should build with ruby1.8-dev 1.8.6.114-1 on armel.
    (Closes: #470194)

 -- akira yamada <akira@debian.org>  Thu, 10 Apr 2008 10:06:10 +0900

libuconv-ruby (0.4.12-2) unstable; urgency=low

  * dropped ruby1.6 support. (Closes: #367911)

 -- akira yamada <akira@debian.org>  Tue, 23 May 2006 15:10:32 +0900

libuconv-ruby (0.4.12-1) unstable; urgency=low

  * new upstream version.

 -- akira yamada <akira@debian.org>  Sat, 14 Aug 2004 19:10:10 +0900

libuconv-ruby (0.4.11-1) unstable; urgency=low

  * new sub-package libuconv-ruby1.6.
      - renamed from libuconv-ruby.
  * new sub-package libuconv-ruby1.8.
  * Standards-Version: 3.6.1.

 -- akira yamada <akira@debian.org>  Wed, 10 Sep 2003 15:47:08 +0900

libuconv-ruby (0.4.10-1) unstable; urgency=low

  * new upstream version.
  * excutes extconf.rb with --enable-fullwidth-reverse-solidus.

 -- akira yamada <akira@debian.org>  Wed,  2 Oct 2002 17:06:27 +0900

libuconv-ruby (0.4.9-1) unstable; urgency=low

  * new upstream version.

 -- akira yamada <akira@debian.org>  Tue, 12 Feb 2002 11:19:08 +0900

libuconv-ruby (0.4.8-1) unstable; urgency=low

  * upgraded to new upstream version.

 -- akira yamada <akira@debian.org>  Tue, 11 Dec 2001 09:27:20 +0900

libuconv-ruby (0.4.6-1) unstable; urgency=low

  * upgraded to new upstream version.

 -- akira yamada <akira@debian.org>  Fri, 16 Mar 2001 16:50:09 +0900

libuconv-ruby (0.4.5-1) unstable; urgency=low

  * upgraded to new upstream version.

 -- akira yamada <akira@debian.org>  Wed, 31 Jan 2001 05:34:38 +0900

libuconv-ruby (0.4.4-2) unstable; urgency=low

  * rebuild with ruby_1.6.2-5.

 -- akira yamada <akira@debian.org>  Thu, 25 Jan 2001 23:23:35 +0900

libuconv-ruby (0.4.4-1) unstable; urgency=low

  * Upgraded to new upstream version.

 -- akira yamada <akira@debian.org>  Sun, 19 Nov 2000 01:50:02 +0900

libuconv-ruby (0.4.3-2) unstable; urgency=low

  * Rebuild with ruby_1.6.0.
  * Updated Standards-Version to 3.2.1.
  * Added Build-Depends field into control file.

 -- akira yamada <akira@debian.org>  Mon,  4 Sep 2000 17:27:08 +0900

libuconv-ruby (0.4.3-1) unstable; urgency=low

  * Upgraded to new upstream version.
  * Rebuid with ruby_1.4.4

 -- akira yamada <akira@debian.org>  Tue,  4 Apr 2000 21:14:52 +0900

libuconv-ruby (0.4.0-1) unstable; urgency=low

  * Upgraded to new upstream version:
    - CP932 supported.

 -- akira yamada <akira@debian.org>  Tue, 30 Nov 1999 23:33:27 +0900

libuconv-ruby (0.3.1-3) unstable; urgency=low

  * FHS complience.

 -- akira yamada <akira@debian.org>  Thu, 21 Oct 1999 23:25:02 +0900

libuconv-ruby (0.3.1-2) unstable; urgency=low

  * Build with ruby 1.4.

 -- akira yamada <akira@debian.org>  Wed, 18 Aug 1999 10:21:06 +0900

libuconv-ruby (0.3.1-1) unstable; urgency=low

  * Upgraded to new upstream version.

 -- akira yamada <akira@debian.org>  Tue, 06 Jul 1999 21:26:57 +0900

ruby-uconv-module (0.3.0-1) unstable-jp; urgency=low

  * Upgraded to new upstream version.
      - Support Japanese supplement characters.
      - Support UCS-4 and UTF-16.

 -- akira yamada <akira@linux.or.jp>  Thu, 25 Feb 1999 17:11:21 +0900

ruby-uconv-module (0.2.1-3) unstable-jp; urgency=low

  * Build with ruby1.2_1.2.2.

 -- akira yamada <akira@linux.or.jp>  Mon,  1 Feb 1999 14:55:13 +0900

ruby-uconv-module (0.2.1-2) unstable-jp; urgency=low

  * Build with ruby_1.2.2.

 -- akira yamada <akira@linux.or.jp>  Fri, 22 Jan 1999 17:58:01 +0900

ruby-uconv-module (0.2.1-1) unstable-jp; urgency=low

  * Upgraded to new upstream version.
    - Added README file written in English.

 -- akira yamada <akira@linux.or.jp>  Fri, 21 Aug 1998 03:17:47 +0900

ruby-uconv-module (0.2-1) unstable-jp; urgency=low

  * Initial Release.

 -- akira yamada <akira@linux.or.jp>  Fri, 14 Aug 1998 22:54:51 +0900
