## Uconv/EUCJP-Unicode
## Copyright 2004 by yoshidam
##

require 'uconv'

module Uconv
  EUCJP_UNICODE_UCS_MAP = {
    "\xa1\xc0"=>0x005c
  }
  UCS_EUCJP_UNICODE_MAP = {
     0xff3c=>""
  }


  def self.eucjp_unicode()
    self.euc_hook = proc do |e|
      EUCJP_UNICODE_UCS_MAP[e]
    end

    self.unicode_euc_hook = proc do |u|
      UCS_EUCJP_UNICODE_MAP[u]
    end
  end
end

Uconv.eucjp_unicode
