ARGV.push('-v')
require 'test/unit'
require 'uconv'

class UconvTest < Test::Unit::TestCase

  def test_utf8_utf16_roundrip
    assert_equal "á", Uconv.u16tou8(Uconv.u8tou16("á"))
  end

end
