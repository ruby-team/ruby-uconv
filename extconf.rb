require 'mkmf'

$CFLAGS = ""
$objs = ['uconv.' + $OBJEXT, 'ustring.' + $OBJEXT]

if enable_config("euc", true)
  $CFLAGS += " -DUSE_EUC"
  $objs.push('e2u.' + $OBJEXT , 'u2e.' + $OBJEXT)
end
if enable_config("sjis", true)
  $CFLAGS += " -DUSE_SJIS"
  $objs.push('s2u.' + $OBJEXT , 'u2s.' + $OBJEXT)
end
if enable_config("win32api", false)
  $CFLAGS += " -DUSE_WIN32API"
end

if enable_config("fullwidth-reverse-solidus", true)
  $CFLAGS += " -DUSE_FULLWIDTH_REVERSE_SOLIDUS"
end

if enable_config("compat-win32api", true)
  $CFLAGS += " -DCOMPAT_WIN32API"
end

if enable_config("thread-local", true)
  $CFLAGS += " -DUCONV_THREAD_LOCAL"
end

if enable_config("utf-32", true)
  $CFLAGS += " -DUTF32"
end

create_makefile("uconv")
